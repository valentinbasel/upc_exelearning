# upc_plantilla_exelearning

## Introducción
template  personalizado de eXeLearning, diseñado para ser integrado con el diseño visual de la Universidad Provincial de Córdoba 

## Detalles

Esta plantilla agrega caracteristicas propias para poder crear Objetos de aprendizaje con un diseño de CSS propio y modificaciones en el editor tinyMCE para incluir botones extras.

## autores

- Lila Pagola
- Valentin Basel

